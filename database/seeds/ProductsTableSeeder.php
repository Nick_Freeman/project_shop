<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            [
                'title' => 'Stabilizator',
                'slug' => 'stabilizator',
                'type'=>'stabilizator',
                'price' => '5',
                'short_description' => 'Super stab',
                'body' => 'lorenz stabilizator super top',
                'image' => 'kotiki.jpg'
            ],
            [
                'title' => 'Stabilizdasds',
                'slug' => 'elex',
                'type'=>'invertor',
                'price' => '10',
                'short_description' => 'Super stabs',
                'body' => 'elex stabilizator super top',
                'image' => 'kotiki.jpg'
            ],
            [
                'title' => 'Stabidasdr',
                'slug' => 'luck',
                'type'=>'invertor',
                'price' => '1',
                'short_description' => 'Super staber',
                'body' => 'luck stabilizator super top',
                'image' => 'kotiki.jpg'
            ],
            [
                'title' => 'Stabilidddr',
                'slug' => 'lorenz',
                'type'=>'invertor',
                'price' => '5',
                'short_description' => 'Super stab',
                'body' => 'lorenz stabilizator super top',
                'image' => 'kotiki.jpg'
            ],
            [
                'title' => 'Stadddd',
                'slug' => 'elex',
                'type'=>'stabilizator',
                'price' => '10',
                'short_description' => 'Super stabs',
                'body' => 'elex stabilizator super top',
                'image' => 'kotiki.jpg'
            ],
            [
                'title' => 'Stabili',
                'slug' => 'luck',
                'type'=>'stabilizator',
                'price' => '1',
                'short_description' => 'Super staber',
                'body' => 'luck stabilizator super top',
                'image' => 'kotiki.jpg'
            ]
        ]);
    }
}
