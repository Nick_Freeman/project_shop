<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('comments')->insert([
            [
                'body' => 'dasdasd',
                'author' => 'Eugen',
                'product_id'  => '1'
            ],
            [
                'body' => 'dasdasd',
                'author' => 'Genry',
                'product_id'  => '2'
            ],
            [
                'body' => 'dasdasd',
                'author' => 'Talib',
                'product_id'  => '3'
            ]
        ]);
    }
}
