$(function() {
    $('#comments').on('submit', function (e) {
        e.preventDefault();
        var form = $(this),
            method = form.attr('method'),
            product_id = form.find('#product_id').val(),
            author = form.find('#author').val(),
            body = form.find('#body').val(),
            data = {

               product_id: product_id,
               author: author,
               body: body
            };
        console.log(data);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('#csrf').val()
            }
        });

        $.ajax({
            url: form.attr('action'),
            type: "POST",
            data: data,
            dataType: 'json',
            success: function(result) {
                console.log(result);
                $('#count').text(1 + parseInt($('#count').text()));
                var listItem = document.createElement('p');
                listItem.innerHTML = "<li class=\"list-group-item\"><h6>"+ result.timeStamp +"</h6><br><h4>"+ result.author +"</h4><br><p><strong>"+ result.body+ "</strong></p></li>";
                $('.currentComment').prepend(listItem);
            },
            error: (error) => {
                alert('SMTH WRONG')

            }
        });

        $('form[id=comments]').trigger('reset');

    });
});
