$(function() {
    $('#products').on('click', function (e) {
        e.preventDefault();
        var form = $(this),
            method = ('method'),
            product_id = form.find('#product_id').val(),
            title = form.find('#title').val(),
            type = form.find('#type').val(),
            price = form.find('#price').val(),
            short_description = form.find('#short_description').val(),
            image = form.find('#image').val(),



            data = {
            product_id: product_id,
            title: title,
            type: type,
            price: price,
            short_description: short_description,
            image: image,


    };
        console.log(data);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('#csrf').val()
            }
        });

        $.ajax({
            url: form.attr('action'),
            type: "POST",
            data: data,
            dataType: 'json',
            success: function(result) {
                console.log(result);
                $('#count').text(1 + parseInt($('#count').text()));
                var listItem = document.createElement('p');
                listItem.innerHTML = "<li class=\"list-group-item\"><h6>"+ result.timeStamp +"</h6><br><h4>"+  +"</h4><br><p><strong>"+ result.body+ "</strong></p></li>";
                $('.currentComment').prepend(listItem);
            },
            error: (error) => {
                alert('SMTH WRONG')

            }
        });

        $('form[id=comments]').trigger('reset');

    });
});
