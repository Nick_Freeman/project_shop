@extends('layouts/main')


@section('jumbotron')

    @if(Auth::check())
        @if(Auth::user()->role == 'admin')
            <div class="btn btn-success">
                <a class="btn btn-success" href="{{route('product.create')}}">CREATE NEW PRODUCT</a>
            </div>
        @endif
    @endif
    <h1 class="display-3">Products</h1>

@endsection
@section('leftmenu')
    <div class="dropdown">
        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Dropdown button
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <a class="dropdown-item" href="#">Action</a>
            <a class="dropdown-item" href="#">Another action</a>
            <a class="dropdown-item" href="#">Something else here</a>
        </div>
    </div>
    <div class="dropdown">
        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Dropdown button
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <a class="dropdown-item" href="#">Action</a>
            <a class="dropdown-item" href="#">Another action</a>
            <a class="dropdown-item" href="#">Something else here</a>
        </div>
    </div>
    <div class="dropdown">
        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Dropdown button
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <a class="dropdown-item" href="#">Action</a>
            <a class="dropdown-item" href="#">Another action</a>
            <a class="dropdown-item" href="#">Something else here</a>
        </div>
    </div>
    <div class="dropdown">
        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton4" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Dropdown button
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <a class="dropdown-item" href="#">Action</a>
            <a class="dropdown-item" href="#">Another action</a>
            <a class="dropdown-item" href="#">Something else here</a>
        </div>
    </div>

@endsection

@section('main_content')
        <div id="main">

        <div id="fon"></div>
        <div id="load"></div>

        <div class="sort">
            Сортировать по: <strong>имени</strong>(<span id="namea">от А до Я</span>/ <span id="named">от Я до А </span>); <strong>цене</strong>(<span id="pricea">возрастание</span>/<span id="priced">убывание</span>)</span>

        </div>

            <div id="tovar">
                <input type="hidden" id="csrf" value="{{csrf_token()}}" />
                <input  name="url" id="url" type="hidden"  value="{{route('product.ajax.table')}}">




                {{--@foreach($products as $product)--}}

        {{--<div class="col-md-4">--}}
            {{--<img src="{{'/uploads/'.$product->image}}" class="img-fluid" alt="Responsive image">--}}
            {{--<h2>{{ $product->title }}</h2>--}}
            {{--<h2>Type:{{ $product->type }}</h2>--}}
            {{--<h2>Price :{{ $product->price }}$</h2>--}}
            {{--<p>{{ $product->short_description }}</p>--}}
            {{--{{$product->created_at->diffForHumans()}}--}}
            {{--<p><a class="btn btn-success" href="{{route('product.show', ["product" => $product->id])}}" role="button">Open product &raquo;</a></p>--}}
            {{--<p><a class="btn btn-info" id=""  role="button">Buy &raquo;</a></p>--}}
            {{--@if(Auth::check())--}}
                {{--@if(Auth::user()->role == 'admin')--}}
                {{--<p><a class="btn btn-warning" href="{{route('product.edit', ["product" => $product->id])}}" role="button">Edit &raquo;</a></p>--}}
                {{--<p><a class="btn btn-danger" id="product" onclick="alertdelete()" role="button">Delete &raquo;</a></p>--}}
                {{--@endif--}}
            {{--@endif--}}
        {{--</div>--}}

    {{--@endforeach--}}
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script type="text/javascript" src="{{ asset('js/prod.js') }}"></script>
  {{-- {{--<script>--}}
        {{--$( document ).ready(function() {--}}
            {{--$.ajax({--}}
                {{--url: "{{route('product.ajax.table')}}",--}}
                {{--data: { _token: '{{csrf_token()}}'},--}}
                {{--method: "POST",--}}
            {{--}).done(function(mass) {--}}
                {{--var result = '';--}}
                {{--for (var i = 0; i < mass.length; i++) {--}}
                    {{--image = '<img src="/uploads/'+mass[i].image+'" class="img-fluid" alt="Responsive image">'--}}
                    {{--result += '<div class="col-md-4">'--}}
                        {{--+image+--}}
                        {{--'<h2>'+mass[i].title+'</h2>' +--}}
                        {{--'<h2>Type:'+mass[i].type+'</h2>' +--}}
                        {{--'<h2>Price:'+mass[i].price+'</h2>' +--}}
                        {{--'<p>Type:'+mass[i].short_description +'</p>' +--}}




                        {{--'</div>';--}}
                    {{--// ещё какие-то выражения--}}
                {{--}--}}

                {{--$('#tovar').append(result)--}}
                {{--//$( this ).addClass( "done" );--}}
            {{--});--}}
            {{--console.log( "ready!" );--}}
        {{--});--}}
    {{--</script>--}}
@endsection






