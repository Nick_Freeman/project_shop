@extends('layouts.main')



@section('jumbotron')

    <div class="col-md-12">
        <h1 class="display-3">{{ $product->title }}</h1>
        <img src="{{'/uploads/'.$product->image}}" class="img-fluid" alt="Responsive image" >

        <p>{{$product->body}}</p>
    </div>
@endsection

@section('main_content')
<div>
    <h2>{{ $product->title }}</h2>
    <h2>Type:{{ $product->type }}</h2>
    <h2>Price :{{ $product->price }}$</h2>
    <p>{{ $product->short_description }}</p>
    <p>{{$product->created_at->diffForHumans()}}</p>
</div>
    <div class="comments col-md-12">
        <h3>Comments (<span id="count">{{count($product->comments)}}</span>):</h3>
        <ul class="list-group">
        @foreach($product->comments as $comment)
            <div>
                <li class="list-group-item">
                <h6>{{$comment->created_at->diffForHumans()}}</h6>
                    <h4> {{$comment->author}}</h4>
                <p><strong>{{$comment->body}}</strong></p>
                </li>
            </div>
        @endforeach
                <div class="currentComment"></div>
        </ul>
    </div>


    <div class="col-md-12 bg-warning" id="cerror">
        @include('layouts.embed.errors')

        <form action="{{route('comment.store',['product_id' => $product->id])}}" method="post" id="comments">

            {{csrf_field()}}

            <div class="form-group">

                <label for="body" style="margin-top: 8px ">Comment product:</label>

                <textarea name="author" id="author"  class="form-control" placeholder="Fill your name:"></textarea>

                <textarea name="body" id="body"  class="form-control" placeholder="Fill your comment:"></textarea>

                <input  name="product_id" id="product_id" type="hidden"  value="{{ $product->id }}">
                <input type="hidden" id="csrf" value="{{csrf_token()}}" />

                <button type="submit" class="btn btn-success" id="sendComment" style="margin-top: 15px ">Product comment</button>
            </div>


        </form>
    </div>
@endsection