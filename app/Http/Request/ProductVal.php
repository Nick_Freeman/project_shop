<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class ProductVal extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {


        switch (Request::route()->getName()){
            case 'product.update':
                return [
                    'id' => 'exists:products,id',
                    'title' => 'required|max:50',
                    'slug' => 'required|max:50',
                    'type' => 'required|max:50',
                    'price' => 'required|numeric',
                    'short_description' => 'required|max:50',
                    'body' => 'required|max:233',
                ];
                break;
            case 'product.store':
                return [
                    'title' => 'required|max:50',
                    'slug' => 'required|max:50',
                    'type' => 'required|max:50',
                    'price' => 'required|numeric',
                    'short_description' => 'required|max:50',
                    'body' => 'required|max:233'
                ];
                break;

        };

    }
    //php artisan make:request StoreBlogPost
    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */

    public function messages()
    {
        return [
            'title.max' => 'Field is required,and max length 50 symbol',
            'slug' => 'Field is required,and max length 50 symbol',
            'type' => 'Field is required,and max length 50 symbol',
            'short_description.max' => 'Field is required,and max length 50 symbol',
            'body.max' => 'Field is required,and max length 233 symbol',
        ];
    }
}