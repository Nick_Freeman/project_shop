<?php

namespace App\Http\Controllers;

use App\Product;
use App\Http\Requests\ProductVal;
use Illuminate\Http\Request;

class ProductsController extends BaseController
{
    public function __construct()
    {
        $this->model = 'App\Product';
        $this->rootView = 'products';
        $this->outPutVariable = 'products';
        $this->outPutVariableForSingle = 'product';
        $this->baseRouteName = 'product';
    }


    public function update(ProductVal $request){

        $product = Product::find($request->id);

        if($product){

            $image = request()->file('image');
            $image->move(public_path().'/uploads', $image->getClientOriginalName());

            $product->fill($request->only('id','title','slug','short_description','body' ,'type','price'));
            $product->image = $image->getClientOriginalName();
            $product->save();

            return redirect()->route('product.index') ;
        } else {
            return redirect()->back();
        }
    }

    public function tableToAjax()
    {
        return response(Product::all());
    }

    public function store(ProductVal $request){

        $image = request()->file('image');
        //$xls = request()->file('nickXLS');
        $image->move(public_path().'/uploads', $image->getClientOriginalName());

        $product = new Product();
        $product->fill($request->only('title','short_description','body','image','slug','type','price'));
        $product->image = $image->getClientOriginalName();
        $product->save();

        return  json_encode([
            'title'=>$product->title,
            'short_description' => $product->short_description,
            'body'=>$product->body,
            'image'=>$product->image,
            'slug'=>$product->slug,
            'type'=>$product->type,
            'price' =>$product->price,
        ]);

        request()->session()->flash('status', 'Product created successfully!');
        return redirect()->route('product.index') ;
    }
}
