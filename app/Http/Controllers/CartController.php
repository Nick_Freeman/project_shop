<?php
namespace App\Http\Controllers;

use App\Cart;
use App\Product;
use Illuminate\Http\Request;

class CartController extends Controller
{
    public function index()
    {
        $cart = Cart::getCart();
        if(count($cart) < 0){
            return redirect('/');
        }
        $products = Cart::getCartProducts($cart);


        return view('cart')->with(compact('cart','products'));
    }

    public function store(Product $product)
    {
        $cart = Cart::addProduct($product);

        return back()->withCookie('cart', json_encode($cart));
    }

    public function remove(Product $product){

        $cart = Cart::removeProduct($product);

        return back()->withCookie('cart', json_encode($cart));
    }


    }
