<?php

namespace App\Http\Controllers;


use App\AboutUs;
use Illuminate\Http\Request;

abstract class BaseController extends Controller
{
    protected $model;
    protected $rootView;
    protected $outPutVariable;
    protected $outPutVariableForSingle;
    protected $baseRouteName;

    public function index(){
        $model = $this->model::all();
        return view($this->rootView.'.index')->with($this->outPutVariable,$model);
    }


    public function show($id){
        $model = $this->model::find($id);
        return view($this->rootView.'.show')->with($this->outPutVariableForSingle,$model);
    }

    public function edit($id){
        $model = $this->model::find($id);
        return view($this->rootView.'.edit')->with($this->outPutVariableForSingle,$model);
    }

    public function create(){
        return view($this->rootView.'.create');
    }

    public function delete($id){
        $model = $this->model::find($id);
        return view($this->rootView.'.delete')->with($this->outPutVariableForSingle,$model);
    }

    public function destroy($id){

        $this->model::find($id) -> delete();

        return redirect()->route($this->baseRouteName.'.index') ;
    }
}
