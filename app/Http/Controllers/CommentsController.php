<?php

namespace App\Http\Controllers;

use App\Product;
use App\Comment;
use App\Http\Requests\CommentVal;
use Illuminate\Http\Request;

class CommentsController extends BaseController
{
    public function __construct()
    {
        $this->model = 'App\Comment';
        $this->rootView = 'comments';
        $this->outPutVariable = 'comments';
        $this->outPutVariableForSingle = 'comment';
        $this->baseRouteName = 'comment';
    }



    public function store(CommentVal $request){

        $comment = new Comment();

        $comment->fill($request->only(  'body','product_id','author'));

        $comment->save();

        return  json_encode([
            'body'=>$comment->body,
            'author' => $comment->author,
            'product_id'=>$comment->product_id,
        ]);




    }
}
