<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $products = DB::table('products')->get();
    return view('index')->with('products', $products);
});


/** PRODUCTS ROUTES */
Route::group(['prefix' => 'product'], function () {

    Route::get('/', 'ProductsController@index') ->name('product.index');
    Route::get('/show/{product}', 'ProductsController@show') ->name('product.show');
    Route::post('ajaxTable','ProductsController@tableToAjax') ->name('product.ajax.table');
});
Route::group(['prefix' => 'product', 'middleware' => ['role']], function () {

    Route::get('/create', 'ProductsController@create') ->name('product.create');
    Route::put('/store', 'ProductsController@store') ->name('product.store');
    Route::get('/edit/{product}', 'ProductsController@edit') ->name('product.edit');
    Route::post('/update/{product}', 'ProductsController@update') ->name('product.update');
    Route::get('/delete/{product}', 'ProductsController@delete') ->name('product.delete');
    Route::delete('/destroy/{product}', 'ProductsController@destroy') ->name('product.destroy');
});

/** COMMENTS ROUTES */
Route::group(['prefix' => 'comment', 'middleware' => ['role']], function () {

    Route::post('/store/{product_id}', 'CommentsController@store')->name('comment.store');

});

/** USER ROUTES*/
Route::group(['prefix' => 'registration'], function () {

    Route::get('/create', 'RegistrationController@create')->name('registration.create');
    Route::put('/store', 'RegistrationController@store')->name('registration.store');

});

/**SESSION ROUTES*/
Route::group(['prefix' => 'session'], function () {

    Route::get('/destroy', 'SessionsController@destroy')->name('session.destroy');
    Route::get('/create','SessionsController@create')->name('session.create');
    Route::post('/store','SessionsController@store')->name('session.store');
});

 /** Dashboard*/
Route::group(['prefix' => 'admin'], function () {

    Route::get('/admin', 'admin\IndexController@index')->name('admin.index');
    Route::delete('/admin/orders/{order}/{product}', 'admin\OrdersController@removeProduct')->name('admin.remove');
});

/** CART ROUTES */
Route::group(['prefix' => 'cart'], function () {

    Route::get('/', 'CartController@index')->name('cart.index');
    Route::get('/store', 'CartController@store')->name('cart.store');
    Route::get('/remove/{cart}', 'CartController@remove')->name('cart.remove');

});
Route::group(['prefix' => 'order'], function () {

    Route::get('/create}', 'OrdersController@create')->name('order.create');
    Route::post('/store', 'OrdersController@store')->name('order.store');
});




//